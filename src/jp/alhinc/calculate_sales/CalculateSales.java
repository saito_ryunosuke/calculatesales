package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	//支店コードの正規表現
	private static final String NUM_REGEXP = "^[0-9]{3}";

	//商品コードの正規表現
	private static final String CHARACTER_REGEXP = "^[A-Za-z0-9]{8}";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String COMMODITY_FILE_NOT_EXIST = "店舗定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String COMMODITY_FILE_INVALID_FORMAT = "商品定義ファイルのフォーマットが不正です";
	private static final String FORMAT_ERROR = "のフォーマットが不正です";
	private static final String BRANCH_CODE_ERROR = "の支店コードが不正です";
	private static final String COMMODITY_CODE_ERROR = "の商品コードが不正です";
	private static final String SALE_NAME_ERROR = "売上ファイル名が連番になっていません";
	private static final String SALE_OVER_DIGITS = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		//コマンドライン引数が渡されているかを確認
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, FILE_INVALID_FORMAT, FILE_NOT_EXIST, NUM_REGEXP)) {
			return;
		}
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, COMMODITY_FILE_INVALID_FORMAT, COMMODITY_FILE_NOT_EXIST, CHARACTER_REGEXP)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		//数字8桁のrcdファイルを検出
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//rcdファイルが連番になっているか確認
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(SALE_NAME_ERROR);
				return;
			}
		}

		//各rcdファイルのデータをbranchSalesにput
		for (int i = 0; i < rcdFiles.size(); i++) {
			File rcdFile = rcdFiles.get(i);
			BufferedReader br = null;
			try {
				FileReader fr = new FileReader(rcdFile);
				br = new BufferedReader(fr);

				//読み込んだrcdファイルを1行ずつ読み込み、リストに入れる
				String line;
				List<String> item = new ArrayList<>();

				while ((line = br.readLine()) != null) {
					item.add(line);
				}

				//rcdファイルのフォーマットが正しいかを確認
				if (item.size() != 3) {
					System.out.println(rcdFile.getName() + FORMAT_ERROR);
					return;
				}

				//支店コードが存在するかを確認
				if (!branchNames.containsKey(item.get(0))) {
					System.out.println(rcdFile.getName() + BRANCH_CODE_ERROR);
					return;
				}

				//商品コードが存在するかを確認
				if (!commodityNames.containsKey(item.get(1))) {
					System.out.println(rcdFile.getName() + COMMODITY_CODE_ERROR);
					return;
				}

				//売上金額が数字か確認
				if (!item.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				Long sales = Long.parseLong(item.get(2));

				//売り上げ金額が10桁を超えていないかを確認
				Long branchSaleAmount = branchSales.get(item.get(0)) + sales;
				Long commoditySaleAmount = commoditySales.get(item.get(1)) + sales;
				if ((branchSaleAmount >= 10000000000L) &&  (commoditySaleAmount >= 10000000000L)) {
					System.out.println(SALE_OVER_DIGITS);
					return;
				}

				//店舗コードが等しいところにputする

				branchSales.put(item.get(0), branchSaleAmount);
				commoditySales.put(item.get(1), commoditySaleAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;

					}

				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> sales, String formatError, String existsError, String numOrCharacter) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//ファイルの存在を確認
			if (!file.exists()) {
				System.out.println(existsError);
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] item = line.split(",");
				//支店定義ファイルと商品定義ファイルのフォーマットの確認
					if ((item.length != 2) || (!item[0].matches(numOrCharacter))) {
						System.out.println(formatError);
						return false;
					}



				names.put(item[0], item[1]);
				sales.put(item[0], (long) 0);
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		File branchOut = new File(path, fileName);

		BufferedWriter bw = null;
		try {
			FileWriter fw = new FileWriter(branchOut);
			bw = new BufferedWriter(fw);

			for (String key : names.keySet()) {
				String sale = String.valueOf(sales.get(key));
				bw.write(key + "," + names.get(key) + "," + sale);
				bw.newLine();

			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}
